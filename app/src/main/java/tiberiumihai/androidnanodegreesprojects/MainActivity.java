package tiberiumihai.androidnanodegreesprojects;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;


public class MainActivity extends Activity {

    Context mainContext = this;

    Button spotifyapp = null;
    Button scoresapp = null;
    Button libraryapp = null;
    Button buildapp = null;
    Button readerapp = null;
    Button capstoneapp = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        spotifyapp  = (Button) findViewById(R.id.spotifyapp);
        scoresapp   = (Button) findViewById(R.id.scoresapp);
        libraryapp  = (Button) findViewById(R.id.libraryapp);
        buildapp    = (Button) findViewById(R.id.buildapp);
        readerapp   = (Button) findViewById(R.id.readerapp);
        capstoneapp = (Button) findViewById(R.id.capstoneapp);

        setOnClickListeners();
    }

    public void setOnClickListeners() {
        // Defining a OnClickListener for all the buttons in view
        View.OnClickListener buttonListener = new View.OnClickListener() {
            public void onClick(View v) {
                // v should be one of the buttons
                // testing for exactly the same object
                if (v == spotifyapp) {
                    Toast.makeText(mainContext, "This button will launch my Spotify App", Toast.LENGTH_SHORT).show();
                } else if (v == scoresapp) {
                    Toast.makeText(mainContext, "This button will launch my Football Scores App", Toast.LENGTH_SHORT).show();
                } else if (v == libraryapp) {
                    Toast.makeText(mainContext, "This button will launch my Library App", Toast.LENGTH_SHORT).show();
                } else if (v == buildapp) {
                    Toast.makeText(mainContext, "This button will launch my Build It Bigger App", Toast.LENGTH_SHORT).show();
                } else if (v == readerapp) {
                    Toast.makeText(mainContext, "This button will launch my XYZ Reader App", Toast.LENGTH_SHORT).show();
                } else if (v == capstoneapp) {
                    Toast.makeText(mainContext, "This button will launch my Capstone App", Toast.LENGTH_SHORT).show();
                }
            }
        };

        // setting the OnClickListener for each button
        spotifyapp.setOnClickListener(buttonListener);
        scoresapp.setOnClickListener(buttonListener);
        libraryapp.setOnClickListener(buttonListener);
        buildapp.setOnClickListener(buttonListener);
        readerapp.setOnClickListener(buttonListener);
        capstoneapp.setOnClickListener(buttonListener);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Toast.makeText(mainContext, "Settings", Toast.LENGTH_SHORT).show();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
